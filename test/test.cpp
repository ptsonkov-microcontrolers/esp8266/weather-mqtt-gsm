#include <Arduino.h>
#include <SoftwareSerial.h>

// Create software serial object to communicate with SIM800L
SoftwareSerial mySerial(D5, D6); // RX, TX

// Array with AT commands and descriotions
String cmdList[][2]{
    // {"AT", "Check modem communication with NodeMCU"},
    // {"AT+CCID", "Read SIM information to confirm whether the SIM is
    // plugged"},
    // {"AT+CMEE=2", "Enable debug"},
    // {"AT+CGMI", "Print manifacturer"},
    // {"AT+CGMM", "Print model"},
    // {"AT+CGMR", "Print revision"},
    // {"AT+CGSN", "Print IMEI"},
    // {"AT+CNUM", "Subscriber number"},
    // {"AT+GCAP", "Supported features"},
    {"AT+CFUN?", "Check device functionality"},
    {"AT+CFUN=1", "Set device functionality"},
    // {"AT+CPAS", "Check if device is ready"},
    {"AT+COPS=?", "Check available networks"},
    {"AT+CSQ", "Signal quality test, value range is 0-31 , 31 is the best"},
    // {"AT+CREG?", "Check whether it has registered in thenetwork"},
    // {"AT+COPS?", "Check that you are connected to the network"},
    // {"AT+CBC", "Check battery state"},
};

void updateSerial();

void setup() {
  delay(5000);

  // Begin serial communication (Serial Monitor)
  Serial.begin(115200);

  // Begin serial communication with SIM800L
  mySerial.begin(57600);
}

void loop() {

  Serial.println();
  Serial.println("Start modem tests...");
  updateSerial();
  Serial.println("==================");
  delay(1000);

  // Loop trough command array
  for (byte i = 0; i < sizeof(cmdList) / sizeof(cmdList[0]); i++) {
    Serial.print("Description:    ");
    Serial.println(cmdList[i][1]);
    // Serial.print("Command:     ");
    // Serial.println(cmdList[i][0]);
    mySerial.println(cmdList[i][0]);
    Serial.print("Send command:   ");
    updateSerial();
    Serial.println("==================");
  }

  Serial.println("End of tests");
  delay(10000);
}

void updateSerial() {
  delay(500);
  // Forward what Serial received to Software Serial Port
  while (Serial.available()) {
    mySerial.write(Serial.read());
  }
  // Forward what Software Serial received to Serial Port
  while (mySerial.available()) {
    Serial.write(mySerial.read());
  }
}
